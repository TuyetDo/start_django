from django.db import models
from django.utils import timezone
# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=100)


class ToDoList(models.Model):
    list_to_do = models.TextField(blank=True)
    date_create = models.DateField(default=timezone.now().strftime("%Y-%m-%d"))
    due_create = models.DateField(default=timezone.now().strftime("%Y-%m-%d"))
    category = models.ForeignKey(Category,on_delete= models.CASCADE)








